# 8mu - chip-8 emulator
# License
GPLv3. Read LICENSE
# Build
```bash
git clone https://Undefined3102@bitbucket.org/Undefined3102/8mu.git
cd 8mu
make
```
# Requirments
SDL2
# Author
undefined3102@gmail.com
